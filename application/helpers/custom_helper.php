<?php
if ( ! function_exists('json_file') )
{
  function json_file($data)
  {
    header('Content-Type: application/json');

    print json_encode($data);
    exit();
  }
}

if ( ! function_exists('discount_price') )
{
  function discount_price($price, $dicount)
  {
    $p = $price - ($price*($dicount/100));
    return $p;
  }
}

if ( ! function_exists('clean_mask') )
{
  function clean_mask($masked_number)
  {
      if ($masked_number != "") {
          $res = str_replace(",", "", $masked_number);

          return $res;
      } else {
          return 0;
      }
  }

}
