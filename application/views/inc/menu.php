<div class="menu-container">
  <div class="btn-group btn-group-justified">
    <a href="<?php echo base_url("dashboard"); ?>" class="btn btn-default <?php if($this->router->fetch_class() == "dashboard") echo "btn-primary bg-prmary"; ?>"><i class="fa fa-2x fa-home"></i><br>Dashboard</a>
    <a href="<?php echo base_url("product"); ?>" class="btn btn-default <?php if($this->router->fetch_class() == "product") echo "btn-primary bg-prmary"; ?>"><i class="fa fa-2x fa-coffee"></i><br>Produk</a>
    <a href="<?php echo base_url("sales"); ?>" class="btn btn-default <?php if($this->router->fetch_class() == "sales") echo "btn-primary bg-prmary"; ?>"><i class="fa fa-2x fa-shopping-cart"></i><br>Penjualan</a>
    <!-- <a href="<?php echo base_url("user"); ?>" class="btn btn-default <?php if($this->router->fetch_class() == "user") echo "btn-primary bg-prmary"; ?>"><i class="fa fa-2x fa-users"></i><br>Users</a> -->
    <a href="<?php echo base_url("report"); ?>" class="btn btn-default <?php if($this->router->fetch_class() == "report") echo "btn-primary bg-prmary"; ?>"><i class="fa fa-2x fa-bar-chart"></i><br>Laporan</a>
    <a href="<?php echo base_url("profile"); ?>" class="btn btn-default <?php if($this->router->fetch_class() == "profile") echo "btn-primary bg-prmary"; ?>"><i class="fa fa-2x fa-user"></i><br>Profile</a>
  </div>
</div>

<!-- <script src="<?php echo base_url(); ?>assets/swipe.js"></script>


<script>
// document.addEventListener('swiped-down', function(e) {
//   var el = e.target.className;
//   console.log(el);
//   if(el){
//     if($("." + el).parents(".page-header").length || el == "page-header"){
//       console.log("Ok");
//       window.location.reload();
//     }
//     // Website2APK.showToast("Swipe Down");
//   }
//
// });
</script> -->
