<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style media="screen">
  body{
    background-image: url('<?php echo base_url("assets/img/bg.jpg"); ?>');
    background-size: cover;
  }
  .login-box{
    width: 400px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 100px;
    margin-bottom: 100px;
  }
  .login-box-body{
    padding: 3px;
  }
  h1.login-title{
    font-family: sans-serif;
    text-align: center;
  }
  .login-box-msg{
    text-align:center;
  }
  </style>
</head>
<body>

<form action="<?php echo base_url("login/auth"); ?>" method="post">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="panel panel-default">
      <div class="panel-heading">
          <h1 class="login-title">Login</h1>
      </div>
      <div class="panel-body">
        <?php switch ($error_message) {
          case 1:
            echo '<p class="login-box-msg text-warning">Authentication Failed!</p>';
            break;

          case 2:
            echo '<p class="login-box-msg text-danger">Username atau Password salah!</p>';
            break;

          case 3:
            echo '<p class="login-box-msg text-warning">Username or Password Empty!</p>';
            break;

          default:
            echo '<p class="login-box-msg">Sign in to start your session</p>';
            break;
        } ?>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="username" autofocus>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
      </div>
      <div class="panel-footer">

        <div class="row">
          <div class="col-xs-8">
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Login In</button>
          </div>
          <!-- /.col -->
        </div>
      </div>
    </div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</form>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script type="text/javascript">
  $("input").focusin(function(event) {
    $(this).select();
  });
</script>
</body>
</html>
