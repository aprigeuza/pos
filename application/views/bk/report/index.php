<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css?t=<?php echo time(); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body>
  <div class="bg"></div>

  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-bar-chart"></i> Report <small></small>
        <div class="pull-right">
          <a href="<?php echo base_url("dashboard"); ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
        </div>
      </h1>
    </div>

    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills" style="background-color:rgb(255, 255, 255);padding:5px;">
          <li class="active"><a data-toggle="pill" href="#home">Laporan Produk</a></li>
          <li><a data-toggle="pill" href="#menu1">Laporan Penjualan</a></li>
        </ul>

        <div class="tab-content">
          <div id="home" class="tab-pane  in active" style="padding-top:15px;">
            <div class="row">
              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading">Laporan Produk
                    <div class="pull-right">
                      <a href="<?php echo base_url("report/report_product_export"); ?>" class="btn btn-default btn-xs"><i class="fa fa-download"></i> Export</a>
                    </div>
                  </div>
                  <div class="panel-body">
                    <table id="table1" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th style="width:12px;">#</th>
                          <th>Nama Produk</th>
                          <th style="width:100px;">Transaksi</th>
                          <th style="width:100px;">Terjual</th>
                          <th style="width:100px;">Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $n = 1;
                        foreach($report_product_list->result() as $row): ?>
                          <tr>
                            <td><?php echo $n++; ?></td>
                            <td><?php echo $row->product_name; ?></td>
                            <td class="text-right"><?php echo number_format($row->total_transaction, 2); ?></td>
                            <td class="text-right"><?php echo number_format($row->sum_order_qty, 2); ?></td>
                            <td class="text-right"><?php echo number_format($row->sum_subtotal, 2); ?></td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="menu1" class="tab-pane " style="padding-top:15px;">
            <div class="row">
              <div class="col-md-2">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Filter</h3>
                  </div>
                  <div class="panel-body">
                    <div class="form-group">
                      <label for="filter_from">Dari</label>
                      <input type="text" class="form-control datepicker" id="filter_from" name="filter_from" value="<?php echo $filter_from; ?>">
                    </div>
                    <div class="form-group">
                      <label for="filter_to">Sampai</label>
                      <input type="text" class="form-control datepicker" id="filter_to" name="filter_to" value="<?php echo $filter_to; ?>">
                    </div>
                    <div class="form-group">
                      <label for="filter_cancel"><input type="checkbox" id="filter_cancel" name="filter_cancel" value="1" <?php if ($filter_cancel): ?>checked<?php endif; ?>> Tampilkan Batal</label>
                    </div>
                    <button type="button" name="button" class="btn btn-block btn-primary" onclick="return getReportSales();">Filter</button>
                  </div>
                </div>
              </div>
              <div class="col-md-10">
                <div class="panel panel-default">
                  <div class="panel-heading">Laporan Penjualan
                    <div class="pull-right">
                      <a href="<?php echo base_url("report/report_sales_export"); ?>" class="btn btn-default btn-xs"><i class="fa fa-download"></i> Export</a>
                    </div>
                  </div>
                  <div class="panel-body" id="report_sales_table">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#table1').DataTable();
  })
</script>
<script>
  function getReportSales(){
    var filter_cancel = 0;
    if($("#filter_cancel").is(":checked")){
      filter_cancel=1;
    }
    $.ajax({
      url: '<?php echo base_url('report/report_sales_ajax'); ?>',
      type: 'GET',
      dataType: 'JSON',
      data: {
        filter_from: $("#filter_from").val(),
        filter_to: $("#filter_to").val(),
        filter_cancel: filter_cancel
      },
      success:function(response){
        $("#report_sales_table").html(response.html);
        $('#table2').DataTable();
      }
    });
  }
  $(document).ready(function() {
    getReportSales();
  });
</script>
<script>
  $(document).ready(function() {
    //Datemask dd/mm/yyyy
    $('#filter_from').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' });
    $('#filter_to').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' });

    //Date picker
    $('.datepicker').datepicker({
      autoclose: true,
      format:"yyyy-mm-dd"
    })
  });
</script>
</body>
</html>
