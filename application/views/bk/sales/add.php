<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Penjualan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css?t=<?php echo time(); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style media="screen">
    .form-control, .btn{
      font-size: 20px;
      height: 42px;
    }
  </style>

</head>
<body>
<div class="bg"></div>
<form action="<?php echo base_url("sales/save"); ?>" method="post">
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-shopping-cart"></i> Penjualan <small>Tambah Data</small>
        <div class="pull-right">
          <a href="<?php echo base_url("sales/add"); ?>" class="btn btn-warning"><i class="fa fa-spinner"></i> Reset [F5]</a>
          <a href="<?php echo base_url("sales"); ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali [F11]</a>
        </div>
      </h1>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tambah Transaksi</h3>
      </div>
      <div class="panel-body">

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="sales_code">Kode Transaksi:</label>
                <input name="sales_code" value="<?php echo $new_sales_code; ?>" type="text" readonly class="form-control" id="sales_code" placeholder="Kode Penjualan" required>
              </div>
              <div class="form-group">
                <label for="sales_date">Tanggal Transaksi:</label>
                <input name="sales_date" value="<?php echo date("Y-m-d"); ?>" readonly type="text" class="form-control" id="sales_date" placeholder="Tanggal" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="well well-lg" style="min-height:134px">
                <h2 class="text-right" id="total_txt">Rp. -</h2>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-4" style="margin-bottom:5px;">
              <select class="selectpicker form-control" name="product" id="product">
                <option value="0">-- Pilih Produk --</option>
                <?php foreach($product_list->result() as $row): ?>
                <option value="<?php echo $row->id; ?>" data-product_id="<?php echo $row->id; ?>" data-product_name="<?php echo $row->product_name; ?>" data-product_price="<?php echo $row->product_price; ?>" data-product_discount="<?php echo $row->product_discount; ?>"><?php echo $row->product_name; ?> (Rp. <?php echo number_format($row->product_price + ($row->product_price * ($row->product_discount /100))); ?>)</option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-md-2" style="margin-bottom:5px;">
              <input type="text" id="product_order_qty" name="" value="1" class="form-control text-right numberFormat" placeholder="Jumlah">
            </div>
            <div class="col-md-6" style="margin-bottom:5px;">
              <button type="button" name="button" class="btn btn-primary" onclick="addProduct();">Tambahkan Produk</button>
              <div class="pull-right">
                <button type="button" class="btn btn-success" onclick="bayar();"><i class="fa fa-money"></i> Bayar [F10]</button>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <div id="sales_product">
                <table id="table1" class="table table-bordered">
                  <thead>
                    <th class="text-center">#</th>
                    <th>Produk</th>
                    <th>Harga</th>
                    <th>Diskon</th>
                    <th>Jumlah</th>
                    <th>Subtotal</th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal_bayar" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Bayar</h4>
        </div>
        <div class="modal-body form-horizontal">
          <div class="form-group">
            <label for="total_nominal" class="control-label col-md-6" style="text-align:left">Total :</label>
            <div class="col-md-6">
              <input type="text" class="form-control text-right" id="total_nominal" name="grand_total" placeholder="" readonly>
            </div>
          </div>
          <div class="form-group">
            <label for="jml_bayar" class="control-label col-md-6" style="text-align:left">Bayar :</label>
            <div class="col-md-6">
              <input type="text" class="form-control text-right numberFormat" id="jml_bayar" name="paid_amount" placeholder="" value="0">
            </div>
          </div>
          <div class="form-group">
            <label for="kembalian" class="control-label col-md-6" style="text-align:left">Kembali :</label>
            <div class="col-md-6">
              <input type="text" class="form-control text-right" id="kembalian" name="paid_change" placeholder="" readonly>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary" name="submit" value="process">Proses & Simpan</button>
          <button type="submit" class="btn btn-success" name="submit" value="process_print">Proses & Cetak</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
  //Initialize Select2 Elements
  $('.select2').select2()

  function number_format (number, decimals, dec_point='.', thousands_sep=',') {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = thousands_sep,
        dec = dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
  }

  (function ( $ ) {
    $.fn.numberFormat = function(options, val){

      if(options=="getValue"){
        if($(this)){
          var elemValue = $(this).val();
          if(elemValue==""){
            return 0;
          }else{
            var unMaskedValue = parseFloat(elemValue.replace(/\,/g, ''));
            return unMaskedValue;
          }
        }
      }

      if(options=="setValue"){
        var precision = parseInt($(this).data("precision"));
        if(precision>=0){
          $(this).val(number_format(val, precision));
        }else{
          $(this).val(number_format(val, 0));
        }
      }

      if(options=="clear"){
        $(this).val('');
      }

      return this.each(function() {

        if(options==null){
          var elem = $( this );
          $(this).keypress(function(e) {
            var elemValue = $(this).val();
            var sElemVal = elemValue.split('.');
            var precision = 0;
            if($(this).data("precision")>=0){
              precision = $(this).data("precision");
            }

            // Allow 0-9 & . (comma)
            if ((e.which < 48 || e.which > 57) && (e.which < 46 || e.which > 46)) {
              return false;
            }
            // Pembatasan Comma hanya 1
            if (e.which == 46) {
              if(sElemVal.length > 1){
                return false;
              }
            }

            if(sElemVal[1] && sElemVal[1].length > precision){
              $(this).val(elemValue.substring(0,elemValue.length-1));
              return false;
            }

          });
          $(this).focusin(function(event) {
            var elemValue = $(this).val();
            var newValue = elemValue.replace(/\,/g, '');
            if(elemValue==0){
              $(this).val('');
            }else{
              $(this).val(newValue);
            }
          });
          $(this).focusout(function(e) {
            var elemValue = $(this).val();
            var precision = 0;
            if($(this).data("precision")>=0){
              precision = $(this).data("precision");
            }
            if(elemValue==0){
              $(this).val(number_format(0, precision));
            }else{
              $(this).val(number_format(elemValue, precision));
            }
          });
          $(this).on('mouseup', function() { $(this).select(); });
        }

      });
    };
  }( jQuery ));

  $(function() {
    $(".numberFormat").numberFormat();
  });

</script>



<script type="text/javascript">
//Date picker
$( function() {
  $('#sales_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
} );
</script>

<script>

  function getSalesProduct(){
    var sales_product = $("#sales_product");

    $.ajax({
      url: '<?php echo base_url("sales/get_sales_product"); ?>',
      type: 'GET',
      dataType: 'JSON',
      success:function(response){
        sales_product.html(response.html)
      }
    });

  }

  function addProduct(){
    var product = $("#product option:selected");

    if(checkProductExists(product.data("product_id"))){
      var idx = newIdx();
      var product_id = product.data("product_id");
      var product_name = product.data("product_name");
      var product_price = product.data("product_price");
      var product_discount = product.data("product_discount");
      var order_qty = $("#product_order_qty").numberFormat("getValue");
      var subtotal = 0;

      if (product.val()==0) {
        alert("Pilih Produk!!!");
        return false;
      }

      createRow(idx, product_id,product_name, product_price, product_discount, order_qty, subtotal)

      $("#product_order_qty").numberFormat("setValue", 1);
    }else{
      alert("Produk Sudah ada!");
    }

    $("#product option").removeAttr("selected");
    $("#product option[value=0]").attr("selected", "selected");
  }

  function createRow(idx, product_id, product_name, product_price, product_discount, order_qty, subtotal){
    var html  = '';
    html += '<tr id="tr'+idx+'">'
    html += '<td style="padding:3px;width:30px" class="text-center">'
    html += '<input type="hidden" class="idx" name="idx[]" value="'+idx+'">'
    html += '<input type="hidden" id="product_id'+idx+'" name="product_id['+idx+']" value="'+product_id+'">'
    html += '<input type="hidden" id="product_name'+idx+'" name="product_name['+idx+']" value="'+product_name+'">'
    html += '<a href="#" class="btn btn-danger btn-sm" role="button" onclick="removeRow('+idx+');">'
    html += '<i class="fa fa-trash"></i>'
    html += '</a>'
    html += '</td>'
    html += '<td>'+product_name+'</td>'
    html += '<td style="padding:3px;width:200px">'
    html += '<input style="" class="form-control text-right numberFormat" type="text" id="product_price'+idx+'" name="product_price['+idx+']" value="'+product_price+'">'
    html += '</td>'
    html += '<td style="padding:3px;width:200px">'
    html += '<input style="" class="form-control text-right numberFormat" type="text" id="product_discount'+idx+'" name="product_discount['+idx+']" value="'+product_discount+'">'
    html += '</td>'
    html += '<td style="padding:3px;width:200px">'
    html += '<input style="" class="form-control text-right numberFormat" type="text" id="order_qty'+idx+'" name="order_qty['+idx+']" value="'+order_qty+'">'
    html += '</td>'
    html += '<td style="padding:3px;width:200px">'
    html += '<input style="" readonly class="form-control text-right numberFormat" type="text" id="subtotal'+idx+'" name="subtotal['+idx+']" value="'+subtotal+'">'
    html += '</td>'
    html += '</tr>'

    $("#table1").append(html);

    $(".numberFormat").numberFormat();

    calculateTotal();
  }

  function removeRow(idx){
    $('#tr'+idx).remove();
    calculateTotal();
  }

  function newIdx(){
    var idx = parseInt($("input.idx:last").val());
    if(!$("input.idx:last").val()){
      idx=0;
    }
    return (idx+1);
  }

  function checkProductExists(product_id){
    var res = true;
    $("input.idx").each(function(index, el) {
      var idx = $(this).val();
      var el_product_id = $("#product_id" + idx).val();
      if(el_product_id == product_id){
        res = false
      }
    });
    return res;
  }

  function calculateTotal(){
    var total = 0;
    $("input.idx").each(function(index, el) {
      var idx = $(this).val();
      var product_price = parseInt($("#product_price" + idx).numberFormat("getValue"));
      var product_discount = parseInt($("#product_discount" + idx).numberFormat("getValue"));
      var order_qty = parseInt($("#order_qty" + idx).numberFormat("getValue"));

      var subtotal = order_qty * (product_price - (product_price * (product_discount / 100)));

      total = total + subtotal;

      $("#subtotal" + idx).numberFormat("setValue", subtotal);

    });
    $("#total_txt").html("Rp. " + number_format(total));
    $("#total_nominal").numberFormat("setValue", total);

    var jml_bayar = $("#jml_bayar").numberFormat("getValue");
    $("#kembalian").numberFormat("setValue", (jml_bayar - total));
  }

  $(document).ready(function() {
    calculateTotal();
  });

  $(".numberFormat").change(function(event) {
    calculateTotal();
  });
  $(".numberFormat").focusout(function(event) {
    calculateTotal();
  });
</script>
<script type="text/javascript">
  $("input").focusin(function(event) {
    $(this).select();
  });
</script>
<script>
  function bayar(){
    $("#modal_bayar").modal({backdrop: 'static'});
    $('#myModal').on('shown.bs.modal', function () {
        $('#jml_bayar').focus();
    });
    $("#modal_bayar").modal("show");
  }

  function reCalculate(){
    calculateTotal();
    console.log("Calculated");
    setTimeout(function(){ reCalculate() }, 1000);
  }
  $(document).ready(function() {
    reCalculate();
  });
</script>
<script type="text/javascript">
var isCtrlHold = false;
var isShiftHold = false;

$(document).keyup(function (e) {

    calculateTotal();

    if (e.which == 17) //17 is the code of Ctrl button
        isCtrlHold = false;
    if (e.which == 16) //16 is the code of Shift button
        isShiftHold = false;
});
$(document).keydown(function (e) {
    if (e.which == 17)
        isCtrlHold = true;
    if (e.which == 16)
        isShiftHold = true;

    ShortcutManager(e);
});

function ShortcutManager(e){
    if (e.which == 121) {
        e.preventDefault();
        bayar();
    }
    if (e.which == 122) {
        e.preventDefault();

        if(confirm("Kembali?"))
          window.location = '<?php echo base_url("sales"); ?>';
    }

    //Ctrl+K:
    if (isCtrlHold && e.which == 78) {
        e.preventDefault();
        window.location = '<?php echo current_url(); ?>';
    }
}
</script>
</body>
</html>
