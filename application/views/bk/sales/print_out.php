<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<htmldir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Print Out <?php echo $sales->sales->sales_code; ?></title>
    <style media="print">
      @page {
        margin: 0;
        width: 55mm;
      }
      body{
        margin: 0;
      }
      .print_body{
        width: 55mm !important;
        margin-left: auto;
        margin-right: auto;
      }
    </style>
    <style>
      body{
        font-size: 9px;
      }
      .print_body{
        width: 55mm;
        margin-left: auto;
        margin-right: auto;
        padding-top: 5mm;
        padding-bottom: 20mm;
      }
      .print_title, .print_detail{
        padding-top: 5px;
        padding-left: 5px;
        padding-right: 5px;
      }
      hr{
        margin-top: 2px;
        margin-bottom: 2px;
      }
      .print_detail .print_table{
        border-top: #000 dashed 1px;
        border-bottom: #000 dashed 1px;
      }
      .print_detail .print_table table{
        width: 50mm;
      }
      .print_title table{
        width: 50mm;
      }
      .border-dash-top{
        border-top: #000 dashed 1px;
      }
    </style>
    <style media="screen">
      body{
        background-color: rgb(196, 196, 196);
      }
      .print_body{
        background-color: #fff;
      }
    </style>
  </head>
  <body>
    <div class="print_body">
      <div class="print_title">
        <table>
          <tr>
            <td align="center" colspan="2">
              DESSOF COFFEE</td>
          </tr>
          <tr>
            <td align="left">
              JL. RAYA SERANG, SENTUL <br>
              BALARAJA, TANGERANG - BANTEN
            </td>
            <td align="right">
              <?php echo $sales->sales->sales_code; ?><br>
              <?php echo date("d/m/Y", strtotime($sales->sales->sales_date)); ?><br>
              <?php echo $sales->sales->username; ?>
            </td>
          </tr>
        </table>
        <hr>
      </div>
      <div class="print_detail">
        <div class="print_table">
          <table>
            <?php foreach($sales->sales_product->result() as $row): ?>
            <tr>
              <td><?php echo $row->product_name; ?></td>
              <td align="right"><?php echo number_format($row->order_qty); ?></td>
              <td align="right"><?php echo number_format(($row->product_price - ($row->product_price * ($row->product_discount/100)))); ?></td>
              <td align="right"><?php echo number_format($row->subtotal); ?></td>
            </tr>
            <?php endforeach; ?>
            <tr>
              <td>&nbsp;</td>
              <td align="right" class="border-dash-top" colspan="2">HARGA JUAL :</td>
              <td align="right" class="border-dash-top"><?php echo number_format($sales->sales->grand_total); ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="right" colspan="2">TUNAI :</td>
              <td align="right"><?php echo number_format($sales->sales->paid_amount); ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="right" colspan="2">KEMBALI :</td>
              <td align="right"><?php echo number_format($sales->sales->paid_change); ?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="print_footer">
        <center>
          TERIMAKASIH <br>TELAH BERBELANJA DITEMPAT KAMI<br>
          SILAHKAN BERKUNJUNG KEMBALI
        </center>
      </div>
    </div>
  <script>
    // window.print();
  </script>
  </body>
</html>
