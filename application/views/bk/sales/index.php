<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Penjualan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css?t=<?php echo time(); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body>
  <div class="bg"></div>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-shopping-cart"></i> Penjualan <small></small>
        <div class="pull-right">
          <a href="<?php echo base_url("sales/add"); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data [F10]</a>
          <a href="<?php echo base_url("dashboard"); ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali [F11]</a>
        </div>
      </h1>
    </div>

    <?php switch ($alert) {
      case 1:
        echo '<p class="login-box-msg text-success">Berhasil menyimpan data!</p>';
        break;

      case 2:
        echo '<p class="login-box-msg text-danger">Gagal menyimpan data!</p>';
        break;

      case 3:
        echo '<p class="login-box-msg text-success">Berhasil membatalkan Penjualan!</p>';
        break;

      case 4:
        echo '<p class="login-box-msg text-danger">Gagal membatalkan Penjualan!</p>';
        break;

      default:
        echo '';
        break;
    } ?>

    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">Data Penjualan</div>
          <div class="panel-body">
            <table id="table1" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Kode</th>
                  <th>Tanggal</th>
                  <th>Total Penjualan</th>
                  <th>Pilihan</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $n = 1;
                foreach($sales_list->result() as $row): ?>
                  <tr>
                    <td><?php echo $n++; ?></td>
                    <td><?php echo $row->sales_code; ?></td>
                    <td><?php echo $row->sales_date; ?></td>
                    <td class="text-right"><?php echo number_format($row->grand_total, 2); ?></td>
                    <td>
                      <a href="<?php echo base_url("sales/edit?id=" . $row->id); ?>">Edit</a> | <a href="<?php echo base_url("sales/cancel?id=" . $row->id); ?>" onclick="return confirm('Batalkan Penjualan?');">Batal</a> | <a target="_blank" href="<?php echo base_url("sales/print_out?id=" . $row->id); ?>">Print</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#table1').DataTable()
  })
</script>
<script type="text/javascript">
var isCtrlHold = false;
var isShiftHold = false;

$(document).keyup(function (e) {
  console.log(e.which);

    if (e.which == 17) //17 is the code of Ctrl button
        isCtrlHold = false;
    if (e.which == 16) //16 is the code of Shift button
        isShiftHold = false;
});
$(document).keydown(function (e) {
    if (e.which == 17)
        isCtrlHold = true;
    if (e.which == 16)
        isShiftHold = true;

    ShortcutManager(e);
});

function ShortcutManager(e){
    if (e.which == 121) {
        e.preventDefault();
        window.location = '<?php echo base_url("sales/add"); ?>';
    }
    if (e.which == 122) {
        e.preventDefault();

        if(confirm("Kembali?"))
          window.location = '<?php echo base_url("dashboard"); ?>';
    }
}
</script>
</body>
</html>
