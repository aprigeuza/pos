<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css?t=<?php echo time(); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>
  <div class="bg"></div>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-home"></i> Dashboard <small></small>
        <div class="pull-right">
          <a href="javascript;" data-toggle="modal" data-target="#modal_change_password" style="color:#FFF;font-size:14px;">Ubah Password</a>
          <a href="<?php echo base_url("logout"); ?>" class="btn btn-danger" onclick="confirm('Yakin akan logout?')"><i class="fa fa-power-off"></i> Logout, <?php echo $this->session->userdata("username"); ?></a>
        </div>
      </h1>
    </div>

    <?php switch ($alert) {
      case 3:
        echo '<p class="login-box-msg text-warning">Ubah password gagal!!!</p>';
        break;

      default:
        echo '';
        break;
    } ?>

    <div class="row">
      <div class="col-md-4">
        <a href="<?php echo base_url("product"); ?>" class="btn btn-primary btn-block btn-dashboard"><i class="fa fa-coffee"></i><br>Produk</a>
      </div>
      <div class="col-md-4">
        <a href="<?php echo base_url("sales"); ?>" class="btn btn-success btn-block btn-dashboard"><i class="fa fa-shopping-cart"></i><br>Penjualan</a>
      </div>
      <div class="col-md-4">
        <a href="<?php echo base_url("report"); ?>" class="btn btn-warning btn-block btn-dashboard"><i class="fa fa-bar-chart"></i><br>Laporan</a>
      </div>
    </div>
  </div>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<form action="<?php echo base_url("dashboard/change_password"); ?>" method="post">
  <div class="modal fade" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id=""></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="">Password Lama</label>
            <input type="password" class="form-control" id="old_password" name="old_password" placeholder="">
          </div>
          <div class="form-group">
            <label for="">Password Baru</label>
            <input type="password" class="form-control" id="new_password" name="new_password" placeholder="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Ubah Password</button>
        </div>
      </div>
    </div>
  </div>
</form>
</body>
</html>
