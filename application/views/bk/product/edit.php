<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Produk</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css?t=<?php echo time(); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body>
  <div class="bg"></div>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-coffee"></i> Produk <small>Ubah Data</small>
        <div class="pull-right">
          <a href="<?php echo base_url("product"); ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
        </div>
      </h1>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Ubah Produk</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="<?php echo base_url("product/update"); ?>" method="post">
              <input type="hidden" name="id" value="<?php echo $product->id; ?>">
              <div class="form-group">
                <label class="control-label col-sm-2" for="product_name">Nama Produk:</label>
                <div class="col-sm-10">
                  <input name="product_name" value="<?php echo $product->product_name ?>" type="text" maxlength="100" class="form-control" id="product_name" placeholder="Nama Produk" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="product_price">Harga:</label>
                <div class="col-sm-10">
                  <input name="product_price" value="<?php echo $product->product_price ?>" type="number" class="form-control" id="product_price" placeholder="Harga" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="product_discount">Diskon:</label>
                <div class="col-sm-10">
                  <input name="product_discount" value="<?php echo $product->product_discount ?>" type="number" class="form-control" id="product_discount" placeholder="Diskon" required max="100">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
