<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Produk</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css?t=<?php echo time(); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body>
  <div class="bg"></div>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-coffee"></i> Produk <small></small>
        <div class="pull-right">
          <a href="<?php echo base_url("product/add"); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
          <a href="<?php echo base_url("dashboard"); ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
        </div>
      </h1>
    </div>

    <?php switch ($alert) {
      case 1:
        echo '<p class="login-box-msg text-success">Berhasil menyimpan data!</p>';
        break;

      case 2:
        echo '<p class="login-box-msg text-danger">Gagal menyimpan data!</p>';
        break;

      case 3:
        echo '<p class="login-box-msg text-success">Berhasil menghapus data!</p>';
        break;

      case 4:
        echo '<p class="login-box-msg text-danger">Gagal menghapus data!</p>';
        break;

      default:
        echo '';
        break;
    } ?>

    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">Data Produk</div>
          <div class="panel-body">
            <table id="table1" class="table table-bordered table-hover">
              <thead>
                <th>#</th>
                <th>Nama Produk</th>
                <th>Harga</th>
                <th>Diskon</th>
                <th>Pilihan</th>
              </thead>
              <tbody>
                <?php
                $n = 1;
                foreach($product_list->result() as $row): ?>
                  <tr>
                    <td><?php echo $n++; ?></td>
                    <td><?php echo $row->product_name; ?></td>
                    <td class="text-right"><?php echo number_format($row->product_price, 2); ?></td>
                    <td class="text-right"><?php echo number_format($row->product_discount, 2); ?></td>
                    <td>
                      <a href="<?php echo base_url("product/edit?id=" . $row->id); ?>">Edit</a> | <a href="<?php echo base_url("product/delete?id=" . $row->id); ?>" onclick="return confirm('Hapus Data?');">Hapus</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#table1').DataTable()
  })
</script>
</body>
</html>
