<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <title>Profile</title>
  <?php $this->load->view("inc/asset_header"); ?>

  <style media="screen">
  .modal {
    text-align: center;
    padding: 0!important;
  }
  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }
  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
    width: 80%;
  }
  .modal-content{
    width: auto;
    margin-left: 5px;
    margin-right: 5px;
    max-width: 500px;
  }
  </style>
</head>
<body>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-home"></i> Profile</h1>
    </div>
    <a href="javascript:;" data-toggle="modal" data-target="#modal_change_password" class="btn btn-default btn-block btn-lg">Ubah Password</a>
    <a href="<?php echo base_url("logout"); ?>" class="btn btn-default btn-block btn-lg">Logout</a>
  </div>
  <?php $this->load->view("inc/menu.php"); ?>
  <?php $this->load->view("inc/asset_footer"); ?>
  <form action="<?php echo base_url("dashboard/change_password"); ?>" method="post">
    <div class="modal fade" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id=""></h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="">Password Lama</label>
              <input type="password" class="form-control" id="old_password" name="old_password" placeholder="">
            </div>
            <div class="form-group">
              <label for="">Password Baru</label>
              <input type="password" class="form-control" id="new_password" name="new_password" placeholder="">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Ubah Password</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</body>
</html>
