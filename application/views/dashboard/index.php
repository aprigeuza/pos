<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <title>Dashboard</title>
  <?php $this->load->view("inc/asset_header"); ?>
</head>
<body>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-home"></i> Dashboard</h1>
    </div>

    <div class="row">
      <div class="col-xs-6">
        <a href="<?php echo base_url("dashboard"); ?>" class="btn btn-default btn-dashboard btn-block"><i class="fa fa-home"></i><br>Dashboard</a>
      </div>
      <div class="col-xs-6">
        <a href="<?php echo base_url("product"); ?>" class="btn btn-default btn-dashboard btn-block"><i class="fa fa-coffee"></i><br>Produk</a>
      </div>
      <div class="col-xs-6">
        <a href="<?php echo base_url("sales"); ?>" class="btn btn-default btn-dashboard btn-block"><i class="fa fa-shopping-cart"></i><br>Penjualan</a>
      </div>
      <!-- <div class="col-xs-6">
        <a href="<?php echo base_url("user"); ?>" class="btn btn-default btn-dashboard btn-block"><i class="fa fa-users"></i><br>Users</a>
      </div> -->
      <div class="col-xs-6">
        <a href="<?php echo base_url("report"); ?>" class="btn btn-default btn-dashboard btn-block"><i class="fa fa-bar-chart"></i><br>Laporan</a>
      </div>
      <div class="col-xs-6">
        <a href="<?php echo base_url("profile"); ?>" class="btn btn-default btn-dashboard btn-block"><i class="fa fa-user"></i><br>Profile</a>
      </div>
    </div>
  </div>
  <?php $this->load->view("inc/menu.php"); ?>
  <?php $this->load->view("inc/asset_footer"); ?>
  <form action="<?php echo base_url("dashboard/change_password"); ?>" method="post">
    <div class="modal fade" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id=""></h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="">Password Lama</label>
              <input type="password" class="form-control" id="old_password" name="old_password" placeholder="">
            </div>
            <div class="form-group">
              <label for="">Password Baru</label>
              <input type="password" class="form-control" id="new_password" name="new_password" placeholder="">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Ubah Password</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</body>
</html>
