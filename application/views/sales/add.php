<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <title>Penjualan</title>
  <?php $this->load->view("inc/asset_header"); ?>

  <style media="screen">
    /* .form-control, .btn{
      font-size: 20px;
      height: 42px;
    } */

    .select2-container .select2-selection--single {
      padding-top: 5px;
      padding-bottom: 5px;
      height: 38px
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow{
      height: 36px
    }

    .modal {
      text-align: center;
      padding: 0!important;
    }
    .modal:before {
      content: '';
      display: inline-block;
      height: 100%;
      vertical-align: middle;
      margin-right: -4px;
    }
    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }
  </style>

</head>
<body>
<form action="<?php echo base_url("sales/save"); ?>" method="post" id="formOrder">
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-shopping-cart"></i> Penjualan <small>Tambah Data</small>
        <div class="pull-right">
          <a href="<?php echo base_url("sales"); ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></a>
        </div>
      </h1>
    </div>
    <div class="row">
      <div class="col-xs-7">
        <div class="form-group">
          <label for="sales_code">Kode Transaksi:</label>
          <input name="sales_code" value="<?php echo $new_sales_code; ?>" type="text" readonly class="form-control" id="sales_code" placeholder="Kode Penjualan" required>
        </div>
      </div>
      <div class="col-xs-5">
        <div class="form-group">
          <label for="sales_date">Tanggal Transaksi:</label>
          <input name="sales_date" value="<?php echo date("Y-m-d"); ?>" readonly style="background-color:#fff" type="text" class="form-control" id="sales_date" placeholder="Tanggal" required>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="form-group">
          <label for="">Pelanggan</label>
          <input type="text" class="form-control" name="customer_name" value="Umum">
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <b>Tambah Produk</b>
    <div class="row">
      <div class="col-xs-7" style="margin-bottom:5px;">
        <select class="form-control" name="product" id="product">
          <option value="0">-- Pilih Produk --</option>
          <?php foreach($product_list->result() as $row): ?>
          <option
            value="<?php echo $row->id; ?>"
            data-product_id="<?php echo $row->id; ?>"
            data-product_name="<?php echo $row->product_name; ?>"
            data-product_price="<?php echo $row->product_price; ?>"
            data-product_discount="<?php echo $row->product_discount; ?>"
            data-product_discount_price="<?php echo discount_price($row->product_price, $row->product_discount); ?>">
            <?php echo $row->product_name; ?> (Rp. <?php echo number_format(discount_price($row->product_price, $row->product_discount)); ?>)</option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="col-xs-3" style="margin-bottom:5px;">
        <input type="number" id="product_order_qty" name="" value="1" class="form-control text-right numberFormat" placeholder="Jumlah">
      </div>
      <div class="col-xs-2" style="margin-bottom:5px;">
        <button type="button" name="button" class="btn btn-primary btn-block" onclick="addProduct();"><i class="fa fa-plus"></i></button>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-12">
        <div id="sales_product">
        </div>
      </div>
    </div>
    <hr>
    <div class="text-right">
      <span>Grand Total : </span><br>
      <span class="" id="total_txt">Rp. -</span>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-12">
        <a href="<?php echo base_url("sales/add"); ?>" class="btn btn-warning"><i class="fa fa-spinner"></i> Reset</a>
        <div class="pull-right">
          <button type="button" class="btn btn-success" onclick="bayar();"><i class="fa fa-money"></i> Selesai/Bayar</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="modal_bayar" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Bayar</h4>
        </div>
        <div class="modal-body form-horizontal">
          <div class="form-group">
            <label for="total_nominal" class="control-label col-md-6" style="text-align:left">Total :</label>
            <div class="col-md-6">
              <input type="text" class="form-control text-right" id="total_nominal" name="grand_total" placeholder="" readonly>
            </div>
          </div>
          <div class="form-group">
            <label for="jml_bayar" class="control-label col-md-6" style="text-align:left">Bayar :</label>
            <div class="col-md-6">
              <input type="number" class="form-control text-right" id="jml_bayar" name="paid_amount" placeholder="" value="0">
            </div>
          </div>
          <div class="form-group">
            <label for="kembalian" class="control-label col-md-6" style="text-align:left">Kembali :</label>
            <div class="col-md-6">
              <input type="text" class="form-control text-right" id="kembalian" name="paid_change" placeholder="" readonly>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-left">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          </div>

          <div class="btn-group">
            <button type="submit" class="btn btn-primary" name="submit" value="process">Proses & Simpan</button>
            <button type="submit" class="btn btn-success" name="submit" value="process_print">Proses & Cetak</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>


<?php $this->load->view("inc/menu.php"); ?>
<?php $this->load->view("inc/asset_footer"); ?>

<script>
  //Date picker
  $( function() {
    $('#sales_date').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  } );
</script>

<script>
  $('#formOrder').submit(function() {

    var k = $("#kembalian").numberFormat("getValue");
    if ( k < 0) {
      if(typeof Website2APK === 'undefined'){
        alert("Jumlah Bayar Kurang!!!");
      }else{
        Website2APK.showToast("Jumlah Bayar Kurang!!!");
      }
      return false;
    }
    return true; // return false to cancel form action
  });

  function getSalesProduct(){
    var sales_product = $("#sales_product");

    $.ajax({
      url: '<?php echo base_url("sales/get_sales_product"); ?>',
      cache: false,
      headers: { "cache-control": "no-cache" },
      type: 'GET',
      dataType: 'JSON',
      success:function(response){
        sales_product.html(response.html)
      }
    });

  }

  function addProduct(){
    var product = $("#product option:selected");

    if(checkProductExists(product.data("product_id"))){
      var idx = newIdx();
      var product_id = product.data("product_id");
      var product_name = product.data("product_name");
      var product_price = product.data("product_price");
      var product_discount = product.data("product_discount");
      var product_discount_price = product.data("product_discount_price");
      var order_qty = $("#product_order_qty").numberFormat("getValue");
      var subtotal = 0;

      if (product.val()==0) {
        if(typeof Website2APK === 'undefined'){
          alert("Pilih Produk!!!");
        }else{
          Website2APK.showToast("Pilih Produk!!!");
        }
        return false;
      }

      createRow(idx, product_id,product_name, product_price, product_discount, product_discount_price, order_qty, subtotal)

      $("#product_order_qty").numberFormat("setValue", 1);
    }else{
      if(typeof Website2APK === 'undefined'){
        alert("Produk Sudah ada!");
      }else{
        Website2APK.showToast("Produk Sudah ada!");
      }
    }

    $("#product option").removeAttr("selected");
    $("#product option[value=0]").attr("selected", "selected");
  }

  function createRow(idx, product_id, product_name, product_price, product_discount, product_discount_price, order_qty, subtotal){
    var html  = '';

    html += '<div class="tr-product-box" id="tr'+idx+'">';
    html += '  <div class="row">';
    html += '    <div class="col-xs-12">';
    html += '      <span class="product-title">'+product_name+'</span>';
    html += '    </div>';
    html += '  </div>';
    html += '  <div class="row">';
    html += '    <div class="col-xs-4">';
    html += '      <span class="product-price">Rp. '+number_format(product_discount_price)+'</span>';
    html += '    </div>';
    html += '    <div class="col-xs-8">';



    html += '    <div class="form-horizontal" role="form">';
    html += '      <div class="row">';
    html += '        <div class="col-xs-6" style="margin-right:0px;">';
    html += '          <div class="form-group">';
    html += '            <label class="control-label col-xs-3" style="padding-top:5px;">Disc:</label>';
    html += '            <div class="col-xs-9">';
    html += '              <input style="" class="form-control text-right numberFormat" type="number" id="product_discount'+idx+'" name="product_discount['+idx+']" value="'+product_discount+'">';
    html += '            </div>';
    html += '          </div>';
    html += '        </div>';
    html += '        <div class="col-xs-6" style="margin-left:0px">';
    html += '          <div class="form-group">';
    html += '            <label class="control-label col-xs-3" style="padding-top:5px;">Qty:</label>';
    html += '            <div class="col-xs-9">';
    html += '              <input style="" class="form-control text-right numberFormat" type="number" id="order_qty'+idx+'" name="order_qty['+idx+']" value="'+order_qty+'">';
    html += '            </div>';
    html += '          </div>';
    html += '        </div>';
    html += '      </div>';
    html += '    </div>';

    // html += '      <input style="" class="form-control text-right numberFormat" type="number" id="order_qty'+idx+'" name="order_qty['+idx+']" value="'+order_qty+'">';
    html += '    </div>';
    html += '  </div>';
    html += '  <a href="javascript:;" class="btn btn-danger btn-xs" role="button" onclick="removeRow('+idx+');">';
    html += '    <i class="fa fa-trash"></i> Hapus';
    html += '  </a>';

    html += '  <input type="hidden" class="idx" name="idx[]" value="'+idx+'">';
    html += '  <input type="hidden" id="product_id'+idx+'" name="product_id['+idx+']" value="'+product_id+'">';
    html += '  <input type="hidden" id="product_name'+idx+'" name="product_name['+idx+']" value="'+product_name+'">';
    html += '  <input style="" readonly="" type="hidden" id="product_price'+idx+'" name="product_price['+idx+']" value="'+product_price+'">';
    // html += '  <input style="" readonly="" type="hidden" id="product_discount'+idx+'" name="product_discount['+idx+']" value="'+product_discount+'">';
    html += '  <input style="" readonly="" type="hidden" id="subtotal'+idx+'" name="subtotal['+idx+']" value="'+subtotal+'">';
    html += '</div>';

    $("#sales_product").append(html);

    $(".numberFormat").numberFormat();

    calculateTotal();
  }
  
  function removeRow(idx){
    if(confirm('Hapus Produk?'))
    {
      $('#tr'+idx).remove();
    }
    calculateTotal();
  }

  function newIdx(){
    var idx = parseInt($("input.idx:last").val());
    if(!$("input.idx:last").val()){
      idx=0;
    }
    return (idx+1);
  }

  function checkProductExists(product_id){
    var res = true;
    $("input.idx").each(function(index, el) {
      var idx = $(this).val();
      var el_product_id = $("#product_id" + idx).val();
      if(el_product_id == product_id){
        res = false
      }
    });
    return res;
  }

  function calculateTotal(){
    var total = 0;
    $("input.idx").each(function(index, el) {
      var idx = $(this).val();
      var product_price = parseInt($("#product_price" + idx).numberFormat("getValue"));
      var product_discount = parseInt($("#product_discount" + idx).numberFormat("getValue"));
      var order_qty = ($("#order_qty" + idx).numberFormat("getValue"));

      var subtotal = order_qty * (product_price - (product_price * (product_discount / 100)));

      total = total + subtotal;

      $("#subtotal" + idx).numberFormat("setValue", subtotal);

    });
    $("#total_txt").html("Rp. " + number_format(total));
    $("#total_nominal").numberFormat("setValue", total);

    var jml_bayar = $("#jml_bayar").numberFormat("getValue");
    $("#kembalian").numberFormat("setValue", (jml_bayar - total));
  }

  $(document).ready(function() {
    calculateTotal();
  });

  $(".numberFormat").change(function(event) {
    calculateTotal();
  });
  $(".numberFormat").focusout(function(event) {
    calculateTotal();
  });
</script>
<script>
  function bayar(){
    $("#modal_bayar").modal({backdrop: 'static'});
    $('#myModal').on('shown.bs.modal', function () {
        $('#jml_bayar').focus();
    });
    $("#modal_bayar").modal("show");
  }

  function reCalculate(){
    calculateTotal();
    console.log("Calculated");
    setTimeout(function(){ reCalculate() }, 1000);
  }
  $(document).ready(function() {
    reCalculate();
  });
</script>
</body>
</html>
