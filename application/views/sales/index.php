<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <title>Penjualan</title>
  <?php $this->load->view("inc/asset_header"); ?>
  <style media="screen">
  .modal {
    text-align: center;
    padding: 0!important;
  }
  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }
  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
  </style>
</head>
<body>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-shopping-cart"></i> Penjualan
        <div class="pull-right">
          <a href="<?php echo base_url("sales/add"); ?>" class="btn btn-default btn-sm"><i class="fa fa-plus"></i></a>
        </div>
      </h1>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <input type="text" class="form-control" name="filterSalesCode" id="filterSalesCode" onkeyup="getData();"  value="" placeholder="Cari Transaksi">
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-xs-12">
        <div class="pull-left">
          <span class="badge badge-success" id="displayFilterDate" data-target="#modalFilter" data-toggle="modal" style="cursor:pointer"></span>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-xs-6">
        <div class="pull-left">
          <div class="">
            <span class="badge badge-success" id="displayTotalSales"></span>
          </div>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="pull-right">
          <button type="button" class="btn btn-default btnPrev"><i class="fa fa-arrow-left"></i></button>
          <button type="button" class="btn btn-default btnNext"><i class="fa fa-arrow-right"></i></button>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-xs-12">
        <div id="listData">
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-xs-12">
        <div class="pull-right">
          <button type="button" class="btn btn-default btnPrev"><i class="fa fa-arrow-left"></i></button>
          <button type="button" class="btn btn-default btnNext"><i class="fa fa-arrow-right"></i></button>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view("inc/menu.php"); ?>
  <?php $this->load->view("inc/asset_footer"); ?>
  <!-- page script -->

  <div id="modalFilter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Filter</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Tanggal</label>
            <input type="text" class="form-control" id="filter_date" name="filter_date" placeholder="" readonly style="background-color:#fff" value="<?php echo date("Y-m-d", strtotime($filter_date)); ?>">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" form="formFilter" onclick="getData();">Filter</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    //Date picker
    $( function() {
      $('#filter_date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
      });
    } );
  </script>


  <script>
    var page = 1;
    var total_page = 1;

    $(".btnNext").click(function(){
      if(page < total_page){
        page = page + 1;
      }
      getData();
    });
    $(".btnPrev").click(function(){
      if(page > 0){
        page = page - 1;
      }
      getData();
    });

    function productBox(data){
      var html = "";
      html += '<div class="list-group-item" style="border-radius:0">';
      html += '  <div class="row">';
      html += '    <div class="col-xs-12">';
      // html += '      <div class="clearfix hr"></div>';
      html += '      <div class="row">';
      html += '        <div class="col-xs-6">';
      html += '      <b>'+data.sales_code+'</b>';
      if(data.customer_name){
        html += '      <br>'+data.customer_name+'';
      }
      html += '      <div class="clearfix"></div>';
      html += '      <span class="badge">'+data.sales_date+'</span>';
      html += '      <span class="badge">Rp. '+number_format(data.grand_total)+'</span>';
      html += '        </div>';
      html += '        <div class="col-xs-6">';
      html += '          <div class="pull-right">';
      html += '            <a class="btn btn-sm btn-primary" href="'+BASE_URL+'sales/edit?id='+data.id +'"><i class="fa fa-edit"></i> Edit</a>';
      html += '            <a class="btn btn-sm btn-danger" href="javascript:;" onclick="cancelData(\''+data.id+'\')"><i class="fa fa-times"></i> Cancel</a>';
      html += '            <a class="btn btn-sm btn-success" href="'+BASE_URL+'sales/print_out?id='+data.id +'"><i class="fa fa-print"></i> Cetak</a>';
      html += '          </div>';
      html += '        </div>';
      html += '      </div>';
      html += '    </div>';
      html += '  </div>';
      html += '</div>';
      return html;
    }

    function getData(){

      $("#displayFilterDate").html($("#filter_date").val());

      $.ajax({
        url: BASE_URL + 'api/sales/get_data',
        type: 'GET',
        dataType: 'json',
        cache: false,
        headers: { "cache-control": "no-cache" },
        data:{
          page:page,
          sales_code:$("#filterSalesCode").val(),
          filter_date:$("#filter_date").val()
        },
        beforeSend:function(){
          // loadingSpinner.show()
        }
      })
      .done(function(response) {
        if(response.data.result.length){
          var html = '';
          $.each(response.data.result, function(i, item) {
            html += productBox(item);
          });
          $("#listData").html(html);
          $("#displayTotalSales").html((response.data.start_at + 1) +'-'+ (response.data.end_at) + ' dari ' + response.data.total_rows);

          total_page = response.data.total_page;

          if(response.data.total_page==0)
          {
            $(".btnPrev").attr("disabled", "disabled");
            $(".btnNext").attr("disabled", "disabled");
          }
          else
          {
            if(response.data.total_page == 1){
              $(".btnPrev").attr("disabled", "disabled");
              $(".btnNext").attr("disabled", "disabled");
            }else{
              $(".btnPrev").removeAttr('disabled');
              $(".btnNext").removeAttr('disabled');

              if(response.data.page == 1){
                $(".btnPrev").attr("disabled", "disabled");
              }

              if(response.data.page >= response.data.total_page){
                $(".btnNext").attr("disabled", "disabled");
              }
            }
          }


        }else{
          $("#listData").html('<div class="alert alert-info" role="alert">Tidak ada transaksi.</div>');
        }
        loadingSpinner.hide();
      })
      .fail(function() {
        loadingSpinner.hide();
      })
      .always(function() {
        loadingSpinner.hide();
      });

    }

    $(document).ready(function() {
      getData();
    });
  </script>

  <script>
    function cancelData(id){
      if(confirm('Batalkan transaksi?')){
        $.ajax({
          url: BASE_URL+'api/sales/cancel',
          type: 'GET',
          cache: false,
          headers: { "cache-control": "no-cache" },
          data: {id:id}
        })
        .done(function() {
          console.log("success");
          getData()
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });

      }
    }
  </script>
</body>
</html>
