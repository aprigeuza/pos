<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<htmldir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Print Out <?php echo $sales->sales->sales_code; ?></title>
    <style media="print">
      @page {
        margin: 0;
        width: 58mm;
      }
      body{
        margin: 0;
      }
      .print_body{
        width: 100% !important;
        margin-left: auto;
        margin-right: auto;
      }
      .noprint{
        display:none
      }
      body{
        font-size: 12px;
      }
      .print_body{
        width: 100%;
        margin-left: auto;
        margin-right: auto;
        padding-top: 5mm;
        padding-bottom: 20mm;
      }
      .print_title, .print_detail{
        padding-top: 5px;
        padding-left: 5px;
        padding-right: 5px;
      }
      hr{
        margin-top: 2px;
        margin-bottom: 2px;
      }
      .print_detail .print_table{
        border-top: #000 dashed 1px;
        border-bottom: #000 dashed 1px;
      }
      .print_detail .print_table table{
        width: 100%;
      }
      .print_title table{
        width: 100%;
      }
      .border-dash-top{
        border-top: #000 dashed 1px;
      }
    </style>
    <style>

    </style>
    <style media="screen">
      body{
        background-color: rgb(196, 196, 196);
      }
      .print_detail .print_table table{
        width: 100%;
      }
      .print_title table{
        width: 100%;
      }
      .border-dash-top{
        border-top: #000 dashed 1px;
      }
      .print_body{
        background-color: #fff;
        width: 100%;
      }

    </style>
  </head>
  <body>
    <div class="print_body">
      <div class="print_title">
        <table>
          <tr>
            <td align="center" colspan="2">
              DESSOF COFFEE</td>
          </tr>
          <tr>
            <td align="left">
              JL. RAYA SERANG, SENTUL <br>
              BALARAJA, TANGERANG - BANTEN
            </td>
            <td align="right">
              <?php echo $sales->sales->sales_code; ?><br>
              <?php echo date("d/m/Y", strtotime($sales->sales->sales_date)); ?><br>
              <?php echo $sales->sales->username; ?>
            </td>
          </tr>
        </table>
        <hr>
      </div>
      <div class="print_detail">
        <div class="print_table">
          <table>
            <?php
            $potongan = 0;
            $gt = 0;
            foreach($sales->sales_product->result() as $row):
              $potongan = $potongan + ($row->product_price * $row->order_qty * ($row->product_discount/100));
              $gt = $gt + ($row->product_price * $row->order_qty);
            ?>
            <tr>
              <td style="width:40%"><?php echo $row->product_name; ?>&nbsp;
                <?php if ($row->product_discount > 0): ?>
                  (Disc. <?php echo number_format($row->product_discount) ?>%)
                <?php endif; ?>
              </td>
              <td align="right"><?php echo number_format($row->order_qty); ?></td>
              <td align="right"><?php echo number_format($row->product_price); ?></td>
              <td align="right"><?php echo number_format($row->product_price * $row->order_qty); ?></td>
            </tr>
            <?php endforeach; ?>
            <tr>
              <td>&nbsp;</td>
              <td align="right" class="border-dash-top" colspan="2">HARGA JUAL :</td>
              <td align="right" class="border-dash-top"><?php echo number_format($gt); ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="right" colspan="2">POTONGAN :</td>
              <td align="right"><?php echo number_format($potongan); ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="right" colspan="2"><b>TOTAL BAYAR :</b></td>
              <td align="right"><b><?php echo number_format($gt - $potongan); ?></b></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="right" colspan="2"><b>TUNAI :</b></td>
              <td align="right"><b><?php echo number_format($sales->sales->paid_amount); ?></b></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="right" colspan="2"><b>KEMBALI :</b></td>
              <td align="right"><b><?php echo number_format($sales->sales->paid_change); ?></b></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="print_footer">
        <center>
          TERIMAKASIH <br>TELAH BERBELANJA DITEMPAT KAMI<br>
          SILAHKAN BERKUNJUNG KEMBALI
        </center>
      </div>
    </div>

    <div class="noprint">
      <br><br><br>
      <center>
        <button type="button" onclick="Website2APK.printPage();">Print</button>
      </center>
    </div>
  <script>
    if(typeof Website2APK === 'undefined'){
      window.print();
    }else{
      Website2APK.printPage();
    }
  </script>
  </body>
</html>
