<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <title>Produk</title>
  <?php $this->load->view("inc/asset_header"); ?>
</head>
<body>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-coffee"></i> Produk
        <div class="pull-right">
          <a href="<?php echo base_url("product/add"); ?>" class="btn btn-default btn-sm"><i class="fa fa-plus"></i></a>
        </div>
      </h1>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <input class="form-control" type="text" name="filterProductName" id="filterProductName" onkeyup="getData();" value="" placeholder="Cari Produk">
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-xs-6">
        <div class="pull-left">
          <span class="badge badge-success" id="displayTotalProduct"></span>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="pull-right">
          <button type="button" class="btn btn-default btnPrev"><i class="fa fa-arrow-left"></i></button>
          <button type="button" class="btn btn-default btnNext"><i class="fa fa-arrow-right"></i></button>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-xs-12">
        <div class="list-group" style="border-radius:0" id="listData"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="pull-right">
          <button type="button" class="btn btn-default btnPrev"><i class="fa fa-arrow-left"></i></button>
          <button type="button" class="btn btn-default btnNext"><i class="fa fa-arrow-right"></i></button>
        </div>
      </div>
    </div>
  </div>
  <!-- <a href="<?php echo base_url("product/add"); ?>" class="btn btn-primary tombol-melayang btn-circle"><i class="fa fa-plus"></i></a> -->
  <?php $this->load->view("inc/menu.php"); ?>
  <?php $this->load->view("inc/asset_footer"); ?>

  <script>
    var page = 1;
    var total_page = 1;

    $(".btnNext").click(function(){
      if(page < total_page){
        page = page + 1;
      }
      getData();
    });
    $(".btnPrev").click(function(){
      if(page > 0){
        page = page - 1;
      }
      getData();
    });

    function productBox(data){
      var html = "";
      html += '<div class="list-group-item" style="border-radius:0">';
      html += '  <div class="row">';
      html += '    <div class="col-xs-12">';
      // html += '      <div class="clearfix hr"></div>';
      html += '      <div class="row">';
      html += '        <div class="col-xs-6">';
      html += '      <b>'+data.product_name+'</b>';
      html += '      <div class="clearfix"></div>';
      html += '      <span class="badge">Rp. '+number_format(discount_price(data.product_price, data.product_discount))+'</span>';
      html += '      <span class="badge">Disc : '+number_format(data.product_discount)+' %</span>';
      html += '        </div>';
      html += '        <div class="col-xs-6">';
      html += '          <div class="pull-right">';
      html += '            <a class="btn btn-sm btn-primary" href="'+BASE_URL+'product/edit?id='+data.id +'"><i class="fa fa-edit"></i> Edit</a>';
      html += '            <a class="btn btn-sm btn-danger" href="javascript:;" onclick="deleteData(\''+data.id+'\')"><i class="fa fa-trash"></i> Hapus</a>';
      html += '          </div>';
      html += '        </div>';
      html += '      </div>';
      html += '    </div>';
      html += '  </div>';
      html += '</div>';
      return html;
    }

    function getData(){
      $.ajax({
        url: BASE_URL + 'api/product/get_data',
        cache: false,
        headers: { "cache-control": "no-cache" },
        type: 'GET',
        dataType: 'json',
        data:{
          page:page,
          product_name:$("#filterProductName").val()
        },
        beforeSend:function(){
          // loadingSpinner.show()
        }
      })
      .done(function(response) {
        if(response.data.result.length){
          var html = '';
          $.each(response.data.result, function(i, item) {
            html += productBox(item);
          });
          $("#listData").html(html);
          $("#displayTotalProduct").html((response.data.start_at + 1) +'-'+ (response.data.end_at) + ' dari ' + response.data.total_rows);

          total_page = response.data.total_page;

          if(response.data.total_page==0)
          {
            $(".btnPrev").attr("disabled", "disabled");
            $(".btnNext").attr("disabled", "disabled");
          }
          else
          {
            if(response.data.total_page == 1){
              $(".btnPrev").attr("disabled", "disabled");
              $(".btnNext").attr("disabled", "disabled");
            }else{
              $(".btnPrev").removeAttr('disabled');
              $(".btnNext").removeAttr('disabled');

              if(response.data.page == 1){
                $(".btnPrev").attr("disabled", "disabled");
              }

              if(response.data.page >= response.data.total_page){
                $(".btnNext").attr("disabled", "disabled");
              }
            }
          }
        }else{
          $("#listData").html('<div class="alert alert-info" role="alert">Belum ada produk.</div>');
        }
        loadingSpinner.hide();
      })
      .fail(function() {
        loadingSpinner.hide();
      })
      .always(function() {
        loadingSpinner.hide();
      });

    }

    $(document).ready(function() {
      getData();
    });
  </script>

  <script>
    function deleteData(id){
      if(confirm('Hapus data?')){
        $.ajax({
          url: BASE_URL+'product/delete',
          type: 'GET',
          data: {id:id}
        })
        .done(function() {
          console.log("success");
          getData()
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });

      }
    }
  </script>
</body>
</html>
