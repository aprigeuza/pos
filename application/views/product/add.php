<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
  <title>Produk</title>
  <?php $this->load->view("inc/asset_header"); ?>
</head>
<body>
  <div class="container">
    <div class="page-header">
      <h1><i class="fa fa-coffee"></i> Produk <small>Tambah Data</small>
        <div class="pull-right">
          <a href="<?php echo base_url("product"); ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></a>
        </div>
      </h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <form action="<?php echo base_url("product/save"); ?>" method="post">
          <div class="form-group">
            <label for="product_name">Nama Produk:</label>
            <input name="product_name" type="text" maxlength="100" class="form-control" id="product_name" placeholder="Nama Produk" required>
          </div>
          <div class="form-group">
            <label for="product_hpp">HPP:</label>
            <input name="product_hpp" value="0" type="number" class="form-control" id="product_hpp" placeholder="HPP" required>
          </div>
          <div class="form-group">
            <label for="product_price">Harga:</label>
            <input name="product_price" value="0" type="number" class="form-control" id="product_price" placeholder="Harga" required>
          </div>
          <div class="form-group">
            <label for="product_discount">Diskon  (%):</label>
            <input name="product_discount" value="0" type="number" class="form-control" id="product_discount" placeholder="Diskon" required max="100">
          </div>
          <div class="form-group">
            <!-- <a href="<?php echo base_url("product"); ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a> -->
            <div class="pull-right">
              <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php $this->load->view("inc/menu.php"); ?>
  <?php $this->load->view("inc/asset_footer"); ?>
</body>
</html>
