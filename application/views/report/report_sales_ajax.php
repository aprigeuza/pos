<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="alert alert-info">
  <p>Menampilkan data dari <?php echo $filter_from; ?> sampai <?php echo $filter_to; ?></p>
</div>
<hr style="margin-bottom:3px;margin-top:3px;">
<table id="table2" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th>Status</th>
      <th>Kode</th>
      <th>Tanggal</th>
      <th>User Input</th>
      <th>Jml Jual</th>
      <th>Cash</th>
      <th>Kembalian</th>
      <th>Harga Jual</th>
      <th>Pilihan</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $n = 1;
    $gt = 0;
    foreach($report_sales_list->result() as $row):
      $gt = $gt + $row->grand_total;
    ?>
      <tr>
        <td><?php echo $n++; ?></td>
        <td class="text-center"><?php echo ($row->cancel==1)? "Batal":"Selesai"; ?></td>
        <td><?php echo $row->sales_code; ?></td>
        <td><?php echo $row->sales_date; ?></td>
        <td><?php echo $row->username; ?></td>
        <td class="text-right"><?php echo number_format($row->sum_order_qty, 2); ?></td>
        <td class="text-right"><?php echo number_format($row->paid_amount, 2); ?></td>
        <td class="text-right"><?php echo number_format($row->paid_change, 2); ?></td>
        <td class="text-right"><?php echo number_format($row->grand_total, 2); ?></td>
        <td class="text-center">
          <a target="_blank" class="btn btn-success" href="<?php echo base_url("sales/print_out?id=" . $row->id); ?>">Print</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<hr style="margin-bottom:3px;margin-top:3px;">
<div class="alert alert-success">
  <h2>Total Penjualan : <span class="pull-right"><?php echo number_format($gt, 2); ?></span> </h2>
</div>
