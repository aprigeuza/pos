<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Laporan Produk</h2>
<table id="table1" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th style="width:12px;">#</th>
      <th>Nama Produk</th>
      <th style="width:100px;">Transaksi</th>
      <th style="width:100px;">Terjual</th>
      <th style="width:100px;">Total</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $n = 1;
    foreach($report_product_list->result() as $row): ?>
      <tr>
        <td><?php echo $n++; ?></td>
        <td><?php echo $row->product_name; ?></td>
        <td class="text-right"><?php echo number_format($row->total_transaction, 2); ?></td>
        <td class="text-right"><?php echo number_format($row->sum_order_qty, 2); ?></td>
        <td class="text-right"><?php echo number_format($row->sum_subtotal, 2); ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
