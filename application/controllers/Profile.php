<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $logged_in = $this->user_model->check_login();
    if(!$logged_in)
    {
      redirect("login?error_message=1");
    }
  }

  public function index()
  {
    $this->load->view("profile/index");
  }



}
