<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("user_model");
    $this->load->model("sales_model");

    $logged_in = $this->user_model->check_login();
    if(!$logged_in)
    {
      redirect("login?error_message=1");
    }
    if(!$this->session->userdata("filter_from"))
    {
      $this->session->set_userdata("filter_from", date("Y-m-d"));
      $this->session->set_userdata("filter_to", date("Y-m-d"));
      $this->session->set_userdata("filter_cancel", false);
    }
  }

  function index()
  {
    $data=array();
    $data["report_product_list"] = $this->sales_model->report_product();
    $data["filter_from"] = $this->session->userdata("filter_from");
    $data["filter_to"] = $this->session->userdata("filter_to");
    $data["filter_cancel"] = $this->session->userdata("filter_cancel");
    $this->load->view('report/index', $data);
  }

  function report_product_export()
  {

    $data["report_product_list"] = $this->sales_model->report_product();


    $table_export = $this->load->view('report/report_product_export', $data, TRUE);

    $this->output
            ->set_content_type('application/vnd-ms-excel')
            ->set_header("Content-Disposition: attachment; filename=report_product_export.xls")
            ->set_header("Pragma: no-cache")
            ->set_output($table_export)
            ->_display();
    exit;
  }

  function report_sales_ajax()
  {

    $this->session->set_userdata("filter_from", $this->input->get("filter_from"));
    $this->session->set_userdata("filter_to", $this->input->get("filter_to"));
    $this->session->set_userdata("filter_cancel", $this->input->get("filter_cancel"));

    $data["report_sales_list"] = $this->sales_model->report_sales();

    $data["filter_from"] = $this->session->userdata("filter_from");
    $data["filter_to"] = $this->session->userdata("filter_to");
    $data["filter_cancel"] = $this->session->userdata("filter_cancel");

    $html = $this->load->view('report/report_sales_ajax', $data, TRUE);
    $response = array('html' => $html);

    $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
    exit;
  }

  function report_sales_export()
  {
    $data["filter_from"] = $this->session->userdata("filter_from");
    $data["filter_to"] = $this->session->userdata("filter_to");
    $data["filter_cancel"] = $this->session->userdata("filter_cancel");

    $data["report_sales_list"] = $this->sales_model->report_sales();


    $table_export = $this->load->view('report/report_sales_export', $data, TRUE);

    $this->output
            ->set_content_type('application/vnd-ms-excel')
            ->set_header("Content-Disposition: attachment; filename=report_product_export.xls")
            ->set_header("Pragma: no-cache")
            ->set_output($table_export)
            ->_display();
    exit;
  }

}
