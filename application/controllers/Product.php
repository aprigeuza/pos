<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("user_model");
    $this->load->model("product_model");

    $logged_in = $this->user_model->check_login();
    if(!$logged_in)
    {
      redirect("login?error_message=1");
    }
  }

  function index()
  {
    $this->load->view('product/index');
  }

  function add()
  {
    $data=array();
    $this->load->view('product/add', $data);
  }

  function save()
  {
    $save = $this->product_model->save();

    if($save)
    {
      redirect("product?alert=1");
    }
    else
    {
      redirect("product?alert=2");
    }
  }

  function edit()
  {
    $data=array();
    $data["product"] = $this->product_model->edit();
    $this->load->view('product/edit', $data);
  }

  function update()
  {
    $update = $this->product_model->update();

    if($update)
    {
      redirect("product?alert=1");
    }
    else
    {
      redirect("product?alert=2");
    }
  }

  function delete()
  {
    $response["status"] = true;
    $response["message"] = "success";

    $delete = $this->product_model->delete();

    if($delete)
    {
      // redirect("product?alert=3");
    }
    else
    {
      // redirect("product?alert=4");
    }
    echo json_encode($response);
  }

}
