<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("user_model");
    $this->load->model("sales_model");
    $this->load->model("product_model");
  }

  public function get_data()
  {
    $sales_model = $this->sales_model->get_data();

    $response["status"] = true;
    $response["message"] = "Success!";
    $response["data"] = $sales_model;
    json_file($response);
  }

  public function cancel()
  {
    $cancel = $this->sales_model->cancel();

    if($cancel)
    {
      $response["status"] = true;
      $response["messsage"] = 'Success';
      json_file($response);
    }
    else
    {
      $response["status"] = false;
      $response["messsage"] = 'Failed';
      json_file($response);
    }
  }

}
