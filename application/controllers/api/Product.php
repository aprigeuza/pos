<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model("user_model");
    $this->load->model("product_model");

    // $logged_in = $this->user_model->check_login();
    // if(!$logged_in)
    // {
    //   $response["status"] = false;
    //   $response["message"] = "Request Blocked!";
    //   json_file($response);
    // }
  }

  public function get_data()
  {
    $product_list = $this->product_model->get_data();

    $response["status"] = true;
    $response["message"] = "Success!";
    $response["data"] = $product_list;
    json_file($response);
  }


}
