<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("user_model");
    $this->load->model("sales_model");
    $this->load->model("product_model");

    $logged_in = $this->user_model->check_login();
    if(!$logged_in)
    {
      redirect("login?error_message=1");
    }
  }

  function index()
  {
    $filter_date = $this->input->get("filter_date");

    $data=array();

    $this->sales_model->filter_date = !empty($filter_date)? $filter_date : date("Y-m-d");
    $data["sales_list"] = $this->sales_model->get_all(50);
    $data["alert"] = $this->input->get("alert");

    $data["filter_date"] = !empty($filter_date)? $filter_date : date("Y-m-d") ;
    $this->load->view('sales/index', $data);
  }

  function add()
  {
    $data=array();
    $data["product_list"] = $this->product_model->get_all();
    $data["new_sales_code"] = $this->sales_model->new_sales_code();
    $this->load->view('sales/add', $data);
  }

  function save()
  {
    $save = $this->sales_model->save();

    if($save)
    {

      if($this->input->post("submit")=="process_print")
      {
        redirect("sales/print_out?id=" . $this->session->userdata("sales_print_id"));
      }
      else
      {
        redirect("sales?alert=1");
      }
    }
    else
    {
      redirect("sales?alert=2");
    }
  }

  function edit()
  {
    $data=array();
    $data["sales"] = $this->sales_model->edit();
    $data["product_list"] = $this->product_model->get_all();
    $this->load->view('sales/edit', $data);
  }

  function update()
  {
    $update = $this->sales_model->update();

    if($update)
    {

      if($this->input->post("submit")=="process_print")
      {
        redirect("sales/print_out?id=" . $this->session->userdata("sales_print_id"));
      }
      else
      {
        redirect("sales?alert=1");
      }

    }
    else
    {
      redirect("sales?alert=2");
    }
  }

  function print_out()
  {
    $data=array();
    $data["sales"] = $this->sales_model->edit();
    $this->load->view('sales/print_out', $data);
  }

}
