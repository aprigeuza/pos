<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("user_model");

    $logged_in = $this->user_model->check_login();
    if(!$logged_in)
    {
      redirect("login?error_message=1");
    }
  }

  function index()
  {
    $data=array();
    $data["alert"] = $this->input->get("alert");
    $this->load->view('dashboard/index', $data);
  }

  function change_password()
  {
    $c = $this->user_model->change_password();
    if($c == true)
    {
      redirect("logout");
    }
    else
    {
      redirect("dashboard/?alert=3");
    }
  }

}
