<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("user_model");
  }

  function index()
  {

    $logged_in = $this->user_model->check_login();
    if($logged_in)
    {
      redirect("dashboard");
    }

    $data = array();

    $data["error_message"] = $this->input->get("error_message");

    $this->load->view("login/index", $data);
  }

  function auth()
  {
    $username = $this->input->post("username");
    $password = $this->input->post("password");

    $this->session->set_userdata("username", $username);
    $this->session->set_userdata("password", md5($password));

    $logged_in = $this->user_model->check_login();

    if($logged_in)
    {
      redirect("dashboard");
    }
    else
    {
      redirect("login?error_message=2");
    }
  }

  function logout()
  {
    $this->session->unset_userdata("username");
    $this->session->unset_userdata("password");
    redirect("login");
  }

}
