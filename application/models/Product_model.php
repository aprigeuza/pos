<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model{

  var $row_per_page = 20;

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function get_all()
  {
    $this->db->order_by("product_name", "ASC");
    return $this->db->get("product");
  }

  function get_data()
  {
    $page = $this->input->get("page");
    $product_name = $this->input->get("product_name");

    $page = $page ? $page : 1;

    // Start Pointer Row
    $start = ($this->row_per_page * $page) - ($this->row_per_page);

    $this->db->select("*");
    $this->db->from("product");
    if($product_name) $this->db->like("product_name", $product_name, "BOTH");

    $db2 = clone $this->db;

    $this->db->order_by("product_name", "ASC");
    $this->db->limit($this->row_per_page, $start);
    $res = $this->db->get();

    // Get Total Result
    $res2 = $db2->get();
    $total_rows = $res2->num_rows();
    $total_page = ceil($total_rows / $this->row_per_page);

    $end = $start + $this->row_per_page;
    if($page >= $total_page)
    {
      $end = $total_rows;
    }

    $result["start_at"] = $start;
    $result["end_at"] = $end;
    $result["page"] = $page;
    $result["row_per_page"] = $this->row_per_page;
    $result["total_page"] = $total_page;
    $result["total_rows"] = $res2->num_rows();
    $result["total_result"] = $res->num_rows();
    $result["result"] = $res->result();
    return $result;
  }

  function edit()
  {
    $id = $this->input->get("id");
    $this->db->where("id", $id);
    $res = $this->db->get("product");

    return $res->row();
  }

  function save()
  {
    $data["product_name"] = $this->input->post("product_name");
    $data["product_price"] = $this->input->post("product_price");
    $data["product_discount"] = $this->input->post("product_discount");
    $data["product_hpp"] = $this->input->post("product_hpp");

    $this->db->insert("product", $data);

    if($this->db->affected_rows()) return true;
    return false;
  }

  function update()
  {
    $data["product_name"] = $this->input->post("product_name");
    $data["product_price"] = $this->input->post("product_price");
    $data["product_discount"] = $this->input->post("product_discount");
    $data["product_hpp"] = $this->input->post("product_hpp");

    $where["id"] = $this->input->post("id");

    $this->db->update("product", $data, $where);

    if($this->db->affected_rows()) return true;
    return false;
  }

  function delete()
  {
    $where["id"] = $this->input->get("id");

    $this->db->delete("product", $where);

    if($this->db->affected_rows()) return true;
    return false;
  }

}
