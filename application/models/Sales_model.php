<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model{

  var $row_per_page = 20;

  public $filter_date;

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function get_user_id()
  {
    $username = $this->session->userdata("username");
    $password = $this->session->userdata("password");

    $this->db->from("user");
    $this->db->where("username", $username);
    $this->db->where("password", $password);
    $res = $this->db->get()->row();

    return $res->id;
  }

  function get_data()
  {
    $page = $this->input->get("page");
    $sales_code = $this->input->get("sales_code");
    $filter_date = $this->input->get("filter_date");

    $page = $page ? $page : 1;

    // Start Pointer Row
    $start = ($this->row_per_page * $page) - ($this->row_per_page);

    $this->db->select("*");
    $this->db->from("sales");
    $this->db->where("cancel", false);
    $this->db->where("sales_date", $filter_date);
    if($sales_code)
    {
      $this->db->like("sales_code", $sales_code, "BOTH");
      $this->db->or_like("customer_name", $sales_code, "BOTH");
    }

    $db2 = clone $this->db;

    $this->db->order_by("sales_code", "DESC");
    $this->db->limit($this->row_per_page, $start);
    $res = $this->db->get();

    // Get Total Result
    $res2 = $db2->get();
    $total_rows = $res2->num_rows();
    $total_page = ceil($total_rows / $this->row_per_page);

    $end = $start + $this->row_per_page;
    if($page >= $total_page)
    {
      $end = $total_rows;
    }

    $result["start_at"] = $start;
    $result["end_at"] = $end;
    $result["page"] = $page;
    $result["row_per_page"] = $this->row_per_page;
    $result["total_page"] = $total_page;
    $result["total_rows"] = $res2->num_rows();
    $result["total_result"] = $res->num_rows();
    $result["result"] = $res->result();
    return $result;
  }

  function get_all($limit=0)
  {
    $this->db->select("sales.*");
    $this->db->from("sales");
    // $this->db->join("sales_product", "sales_product.sales_id=sales.id", "RIGHT");
    $this->db->where("sales.sales_date", $this->filter_date);
    $this->db->where("sales.cancel", false);
    // $this->db->group_by("sales_product.sales_id");
    $this->db->order_by("sales.sales_date", "DESC");
    if($limit>0)$this->db->limit($limit);
    return $this->db->get();
  }

  function get_all_with_cancel()
  {
    $this->db->select("sales.*");
    $this->db->from("sales");
    $this->db->join("sales_product", "sales_product.sales_id=sales.id", "RIGHT");
    $this->db->group_by("sales_product.sales_id");
    return $this->db->get();
  }

  function new_sales_code()
  {
    // Format : TRYYMMXXXX

    $this->db->where("YEAR(sales_date)", date("Y"));
    $this->db->where("MONTH(sales_date)", date("m"));
    $this->db->order_by("sales_code", "DESC");
    $this->db->limit(1);
    $last_row = $this->db->get("sales");

    $new_code = "TR".date("ym")."0001";

    if($last_row->num_rows() == 0)
    {
      $new_code = "TR".date("ym")."0001";
    }
    else
    {
      $row = $last_row->row();

      $num = substr($row->sales_code, 6, 4);
      // Convert to integer + 1
      $new_num = (int)$num + 1;

      $new_code = "TR".date("ym").sprintf("%04d", $new_num);

    }

    return $new_code;
  }

  function edit()
  {
    $res = array();

    $id = $this->input->get("id");
    $this->db->select("sales.*, user.username");
    $this->db->where("sales.id", $id);
    $this->db->join("user", "user.id=sales.user_id");
    $sales = $this->db->get("sales")->row();

    $res["sales"] = $sales;

    $this->db->where("sales_id", $id);
    $sales_product = $this->db->get("sales_product");
    $res["sales_product"] = $sales_product;

    return (object)$res;
  }

  function save()
  {
    $sales_code = $this->input->post("sales_code");
    $sales_date = $this->input->post("sales_date");
    $grand_total = $this->input->post("grand_total");
    $paid_amount = $this->input->post("paid_amount");
    $paid_change = $this->input->post("paid_change");
    $customer_name = $this->input->post("customer_name");

    $idx = $this->input->post("idx");
    $product_id = $this->input->post("product_id");
    $product_name = $this->input->post("product_name");
    $product_price = $this->input->post("product_price");
    $product_discount = $this->input->post("product_discount");
    $order_qty = $this->input->post("order_qty");

    $data["sales_code"] = $sales_code;
    $data["sales_date"] = $sales_date;
    $data["customer_name"] = $customer_name;
    $data["grand_total"] = clean_mask($grand_total);
    $data["paid_amount"] = clean_mask($paid_amount);
    $data["paid_change"] = clean_mask($paid_change);
    $data["user_id"] = $this->get_user_id();
    $data["input_date"] = date("Y-m-d H:i:s");

    if(count($idx) == 0) return false;

    $this->db->insert("sales", $data);

    $sales_id = $this->db->insert_id();

    foreach($idx as $_idx)
    {
      $psd["product_id"] = $product_id[$_idx];
      $psd["product_name"] = $product_name[$_idx];
      $psd["product_price"] = clean_mask($product_price[$_idx]);
      $psd["product_discount"] = clean_mask($product_discount[$_idx]);
      $psd["order_qty"] = clean_mask($order_qty[$_idx]);
      $psd["subtotal"] = ($psd["product_price"] - ($psd["product_price"] * ($psd["product_discount"]/100))) * $psd["order_qty"];
      $psd["sales_id"] = $sales_id;
      $this->db->insert("sales_product", $psd);
    }

    $this->session->set_userdata("sales_print_id", $sales_id);

    if($this->db->affected_rows()) return true;
    return false;
  }

  function update()
  {
    $sales_code = $this->input->post("sales_code");
    $sales_date = $this->input->post("sales_date");
    $grand_total = $this->input->post("grand_total");
    $paid_amount = $this->input->post("paid_amount");
    $paid_change = $this->input->post("paid_change");
    $customer_name = $this->input->post("customer_name");

    $sales_id = $this->input->post("id");

    $idx = $this->input->post("idx");
    $product_id = $this->input->post("product_id");
    $product_name = $this->input->post("product_name");
    $product_price = $this->input->post("product_price");
    $product_discount = $this->input->post("product_discount");
    $order_qty = $this->input->post("order_qty");

    $data["sales_code"] = $sales_code;
    $data["sales_date"] = $sales_date;
    $data["customer_name"] = $customer_name;
    $data["grand_total"] = clean_mask($grand_total);
    $data["paid_amount"] = clean_mask($paid_amount);
    $data["paid_change"] = clean_mask($paid_change);
    $data["user_id"] = $this->get_user_id();

    if(count($idx) == 0) return false;

    $this->db->update("sales", $data, array("id" => $sales_id));

    $this->db->delete("sales_product", array("sales_id" => $sales_id));

    foreach($idx as $_idx)
    {
      $psd["product_id"] = $product_id[$_idx];
      $psd["product_name"] = $product_name[$_idx];
      $psd["product_price"] = clean_mask($product_price[$_idx]);
      $psd["product_discount"] = clean_mask($product_discount[$_idx]);
      $psd["order_qty"] = clean_mask($order_qty[$_idx]);
      $psd["subtotal"] = ($psd["product_price"] - ($psd["product_price"] * ($psd["product_discount"]/100))) * $psd["order_qty"];
      $psd["sales_id"] = $sales_id;
      $this->db->insert("sales_product", $psd);
    }

    $this->session->set_userdata("sales_print_id", $sales_id);
    if($this->db->affected_rows()) return true;
    return false;
  }

  function cancel()
  {
    $where["id"] = $this->input->get("id");

    $data["cancel"] = true;

    $this->db->update("sales", $data, $where);

    if($this->db->affected_rows()) return true;

    return false;
  }

  // Report

  function report_product()
  {
    $this->db->select("*, SUM(order_qty) AS sum_order_qty, SUM(subtotal) AS sum_subtotal, COUNT(sales_id) AS total_transaction");
    $this->db->from("sales_product");
    $this->db->join("sales", "sales_product.sales_id=sales.id", "LEFT");
    $this->db->where("sales.cancel", false);
    $this->db->group_by("product_id");
    $this->db->order_by("SUM(subtotal)", "DESC");
    $res = $this->db->get();

    return $res;
  }

  function report_sales()
  {
    $filter_from = $this->session->userdata("filter_from");
    $filter_to = $this->session->userdata("filter_to");
    $filter_cancel = $this->session->userdata("filter_cancel");

    $this->db->select("sales.*, SUM(sales_product.order_qty) AS sum_order_qty, user.username");
    $this->db->from("sales");
    $this->db->join("sales_product", "sales_product.sales_id=sales.id", "LEFT");
    $this->db->join("user", "user.id=sales.user_id", "LEFT");
    $this->db->where("sales_date >=", $filter_from);
    $this->db->where("sales_date <=", $filter_to);
    if(!$filter_cancel) $this->db->where("sales.cancel", false);
    $this->db->order_by("sales_date", "ASC");
    $this->db->group_by("sales_product.sales_id");
    return $this->db->get();
  }

}
