<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function check_login()
  {
    $username = $this->session->userdata("username");
    $password = $this->session->userdata("password");

    $this->db->from("user");
    $this->db->where("username", $username);
    $this->db->where("password", $password);
    $res = $this->db->get();

    if($res->num_rows())
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  function change_password()
  {
    $username = $this->session->userdata("username");
    $password = $this->session->userdata("password");


    $old_password = $this->input->post("old_password");
    $new_password = $this->input->post("new_password");

    if($old_password == $password)
    {
      $this->db->update("user", array('password'=>$new_password), array("username"=>$username));
    }
    else
    {
      return false;
    }

    if($this->db->affected_rows()) return true;
    return false;
  }

}
